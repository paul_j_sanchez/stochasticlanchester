The Lanchester equations were developed in World War I to evaluate
force-on-force combat  based on the initial force sizes and
the per-capita combat effectiveness of the different force's troops.
Lanchester approximated the expected trajectory as a continuous
process using differential equations.  In reality the forces are
comprised of discrete members, and attrition is a discrete process
which occurs at points in time rather than a continuous process
evolving in continuous time, but the continuous differential equations
approximation was used instead of the discrete formulation because
solutions for the latter were computationally daunting.

With modern computing, it is straightforward to implement both the
discrete nature of attrition and some of the stochastic aspects of
combat.  This software distribution provides three implementations
of a stochastic formulation of Lanchester's "modern warfare" model,
where combatants can target any opponent on the battlefield.  Details
of the logic are described in `StochasticLanchester_description.pdf`.

The three implementations are:

  * `StochasticLanchester.rb`: simulates a single battle and gives
  summary statistics of the outcome.
  * `StochasticLanchesterReps.rb`: performs multiple replications
  of the model, generating one line of summary statistics per replicate.
  * `StochasticLanchesterTrace.rb`: simulates a single battle by
  printing a "trace" of the course of the battle over time.

All three models run until one side or the other has no survivors.
