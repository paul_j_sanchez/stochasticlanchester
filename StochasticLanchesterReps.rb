#!/usr/bin/env ruby -w
# frozen_string_literal: true

def exponentialRV(rate) # generate an Exponential RV given the rate
  -Math.log(rand) / rate
end

if ARGV.empty?
  init_red = (STDERR.print \
    'Enter initial number of Red forces: '; gets).to_i
  redkillbluerate = (STDERR.print \
    'Enter per-capita rate at which Reds kill Blues (b): '; gets).to_f
  init_blue = (STDERR.print \
    'Enter initial number of Blue forces: '; gets).to_i
  bluekillredrate = (STDERR.print \
    'Enter per-capita rate at which Blues kill Reds (a): '; gets).to_f
  reps = (STDERR.print \
    'Enter number of replications to perform: '; gets).to_i
else
  init_red = ARGV.shift.to_i
  redkillbluerate = ARGV.shift.to_f
  init_blue = ARGV.shift.to_i
  bluekillredrate = ARGV.shift.to_f
  reps = ARGV.shift.to_i
end

printf 'InitialRed,RedKillBlueRate,InitialBlue,BlueKillRedRate,'
printf "FinalRed,FinalBlue,Winner,BattleDuration\n"

reps.times do
  t = 0.0
  red = init_red
  blue = init_blue
  printf '%d,%6.5f,%d,%6.5f,', red, redkillbluerate, blue, bluekillredrate
  while red > 0 && blue > 0 # As long as both sides have survivors
    # time of next fatal bullet
    t += exponentialRV((redkillbluerate * red) + (bluekillredrate * blue))
    pRedKill = (bluekillredrate * blue) /
               (bluekillredrate * blue + redkillbluerate * red)
    if rand <= pRedKill # determine who gets hit
      red -= 1
    else
      blue -= 1
    end
  end

  winner = blue > 0 ? 'blue' : 'red'
  printf "%d,%d,%s,%6.5f\n", red, blue, winner, t
end
