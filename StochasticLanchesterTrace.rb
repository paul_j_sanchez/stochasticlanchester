#!/usr/bin/env ruby -w
# frozen_string_literal: true

def exponentialRV(rate)	# generate an Exponential RV given the rate
  -Math.log(rand) / rate
end

if ARGV.empty?
  init_red = (STDERR.print \
    'Enter initial number of Red forces: '; gets).to_i
  redkillbluerate = (STDERR.print \
    'Enter per-capita rate at which Reds kill Blues (b): '; gets).to_f
  init_blue = (STDERR.print \
    'Enter initial number of Blue forces: '; gets).to_i
  bluekillredrate = (STDERR.print \
    'Enter per-capita rate at which Blues kill Reds (a): '; gets).to_f
else
  init_red = ARGV.shift.to_i
  redkillbluerate = ARGV.shift.to_f
  init_blue = ARGV.shift.to_i
  bluekillredrate = ARGV.shift.to_f
end

t = 0.0
red = init_red
blue = init_blue

while red > 0 && blue > 0 # As long as both sides have survivors
  # time of next fatal bullet
  t += exponentialRV((redkillbluerate * red) + (bluekillredrate * blue))
  printf 'Time: %8.3f', t
  pRedKill = (bluekillredrate * blue) /
             (bluekillredrate * blue + redkillbluerate * red)
  if rand <= pRedKill # determine who gets hit
    red -= 1
  else
    blue -= 1
  end
  printf "\tRed survivors: %d\tBlue survivors: %d\n", red, blue
end
puts "End of battle at time #{t}"
winner = blue > 0 ? "Blue" : "Red"
puts "#{winner} wins"
